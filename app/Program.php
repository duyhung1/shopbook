<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
    protected $table="programs";

    public function categories() {
        return $this->belongsToMany('App\Category');
    }

    public function levels() {
        return $this->belongsToMany('App\Level');
    }

    public function organizations() {
        return $this->belongsTo('App\Organization', 'organization_id');
        // return $this->belongsTo(Organization::class, 'organization');
    }

    public function locations() {
        return $this->belongsTo('App\Location', 'location_id');
    }

    public function Users() {
        return $this->belongsTo('App\User', 'user_id');
    }

}
