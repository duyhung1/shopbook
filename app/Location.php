<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $table="locations";
    public function programs() {
        return $this->hasMany('App\Program');
    }
}
