<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('manage-users', function ($user){
            return $user->hasAnyRoles(['admin', 'university','universitysub']);
        });

        Gate::define('manage-organization', function ($user){
            return $user->hasAnyRoles(['admin', 'university']);
        });

        Gate::define('access_useradmin', function($user) {
            return $user->hasRole('admin');
        });

        Gate::define('access_user_university', function($user) {
            return $user->hasRole('university');
        });

        Gate::define('access_user_university_sub', function($user) {
            return $user->hasRole('universitysub');
        });

        Gate::define('create_programs', function($user) {
            return $user->hasAnyRoles(['university','universitysub']);
        });

        Gate::define('edit_programs', function($user) {
            return $user->hasAnyRoles(['university','universitysub']);
        });

        Gate::define('delete_programs', function($user) {
            return $user->hasAnyRoles(['university','universitysub']);
        });


    }

}
