<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\CategoryCreateRequest;
use App\Http\Requests\CategoryEditRequest;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::paginate(10);
        return view('admin.category.index')->with(['categories' => $categories]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryCreateRequest $request)
    {
        if($request->hasFile('thumb')) {
            $filenameWithExt = $request->file('thumb')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('thumb')->getClientOriginalExtension();
            $fileNameToStore = $filename."_".time().".".$extension;
            $path = $request->file('thumb')->storeAs('public/category_thumbs',$fileNameToStore);
        }else {
            $fileNameToStore = 'noimage.jpg';
        }

        $category = new Category;
        $category->name = $request->name;
        $category->thumb = $fileNameToStore;
        $category->ordinal = $request->ordinal;
        $category->description = $request->description;

        $category->save();

        return redirect()->route('admin.category.create')->with('messenger', 'Create Success');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    // public function show(Category $category)
    // {
    //     //
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        return view('admin.category.edit')->with(['category' => $category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryEditRequest $request, Category $category)
    {
        if($request->hasFile('thumb')) {
            $filenameWithExt = $request->file('thumb')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('thumb')->getClientOriginalExtension();
            $fileNameToStore = $filename."_".time().".".$extension;
            $path = $request->file('thumb')->storeAs('public/category_thumbs',$fileNameToStore);
        }

        $category->name = $request->name;
        if($request->hasFile('thumb')) {
            if($category->thumb != "noimage.jpg"){
                Storage::delete('public/category_thumbs/'.$category->thumb);
            }
            $category->thumb = $fileNameToStore;
        }
        $category->ordinal = $request->ordinal;
        $category->description = $request->description;

        $category->save();

        return redirect()->route('admin.category.index')->with('messenger', 'Update Success');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        if($category->thumb != "noimage.jpg"){
            Storage::delete('public/category_thumbs/'.$category->thumb);
        }
        $category->delete();

        return redirect()->route('admin.category.index')->with('messenger', 'Delete Success');
    }
}
