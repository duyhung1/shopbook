<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use App\Organization;
use App\Role;
use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserEditRequest;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Gate;
use Illuminate\Auth\Access\Gate as AccessGate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        if(Gate::allows('access_useradmin')) {
            $users = User::paginate(10);
        }else {
            $current_organization = Auth::user()->organization;
            $users = User::where('organization', '=', $current_organization)->paginate(10);
        }

        // return view('admin.users.index', ['users' => $users]);
        return view('admin.users.index')->with(['users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Gate::allows('access_useradmin')) {
            $organizations = Organization::all();
            $roles = Role::all();
        } else {
            $current_organization = Auth::user()->organization;
            $organizations = Organization::where('id', '=', $current_organization)->get();
            // $current_role = Auth::user()->id;
            // $roles = User::find($current_role)->roles()->get();
            $roles = Role::where('id', '=', 3)->get();
            // $roles = $current_role->find(3);

        }
        // dd($current_role);
        return view('admin.users.create')->with([
            'organizations' => $organizations,
            'roles' => $roles
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserCreateRequest $request)
    {
        if($request->hasFile('thumb')) {
            $filenameWithExt = $request->file('thumb')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('thumb')->getClientOriginalExtension();
            $fileNameToStore = $filename."_".time().".".$extension;
            $path = $request->file('thumb')->storeAs('public/thumbs',$fileNameToStore);
        }else {
            $fileNameToStore = 'noimage.jpg';
        }

        $user = new User;

        $user->name = $request ->name;
        $user->password = bcrypt($request ->password);
        $user->email = $request ->email;
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->gender = $request->gender;
        $user->phone = $request->phone;
        $user->thumb =  $fileNameToStore;
        $user->description = $request->description;
        $user->organization = $request->organization;
        $user->active = $request->active;

        $user->save();

        $user->roles()->sync($request->role);
        return redirect()->route('admin.users.create')->with('messenger', 'Create Success');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        // if(Gate::denies('edit_users')) {
        //     return redirect()->route('admin.users.index')->with('messenger','Not Access');
        // }
        $roles = Role::all();
        $organizations = Organization::all();
        return view('admin.users.edit')->with([
            'user' => $user,
            'organizations' => $organizations,
            'roles' =>$roles
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UserEditRequest $request, User $user)
    {
        if($request->hasFile('thumb')) {
            $filenameWithExt = $request->file('thumb')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('thumb')->getClientOriginalExtension();
            $fileNameToStore = $filename."_".time().".".$extension;
            $path = $request->file('thumb')->storeAs('public/thumbs',$fileNameToStore);
        }

        $user->roles()->sync($request->role);
        $user->name = $request ->name;
        if(isset($request ->password)){
            $user ->password = bcrypt($request ->password);
        }
        $user->email = $request ->email;
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->gender = $request->gender;
        $user->phone = $request->phone;
        if($request->hasFile('thumb')) {
            $user->thumb = $fileNameToStore;
        }
        $user->description = $request->description;
        $user->organization = $request->organization;
        $user->active = $request->active;
        if(isset($request ->password)){
            $user ->password = bcrypt($request ->password);
        }

        $user ->save();



        return redirect()->route('admin.users.index')->with('messenger', 'Update Success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        // if(Gate::denies('delete_users')) {
        //     return redirect()->route('admin.users.index')->with('messenger','Not Access');
        // }
        if($user->thumb != "noimage.jpg"){
            Storage::delete('public/thumbs/'.$user->thumb);
        }
        $user->roles()->detach();
        $user->delete();

        return redirect()->route('admin.users.index')->with('messenger', 'Delete Success');
    }
}
