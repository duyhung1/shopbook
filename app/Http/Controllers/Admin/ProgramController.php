<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Program;
use App\Category;
use App\Http\Requests\ProgramCreateRequest;
use App\Http\Requests\ProgramEditRequest;
use App\Organization;
use App\Level;
use App\Location;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Auth;

class ProgramController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Gate::allows('access_useradmin')) {
            $programs = Program::paginate(10);
            // return view('admin.program.index')->with(['programs' => $programs]);
        }elseif(Gate::allows('access_user_university_sub')) {
            $current_user = Auth::user()->id;
            // dd($current_organization);
            $programs = Program::where('user_id', '=', $current_user)->paginate(10);
            // dd($programs);
        }else {
            $current_organization = Auth::user()->relation_organization->id;
            $programs = Program::where('organization_id', '=', $current_organization)->paginate(10);
        }
        return view('admin.program.index', ['programs' => $programs]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Gate::denies('create_programs')) {
                return redirect()->route('admin.program.index')->with('messenger','Not Access');
        }
        $current_organization = Auth::user()->relation_organization->id;
        $organizations = Organization::where('id', '=', $current_organization)->get();
        // dd($organizations);
        $locations = Location::all();
        $levels = Level::all();
        $categories = Category::all();

        return view('admin.program.create')->with([
            'organizations' => $organizations,
            'levels' => $levels,
            'categories' => $categories,
            'locations' => $locations
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProgramCreateRequest $request)
    {
        $user_id = Auth::user()->id;
        $program = new Program;
        $program->name = $request->name;
        $program->organization_id = $request->organization;
        $program->location_id = $request->location;
        $program->user_id = $user_id;
        $program->length = $request->length;
        $program->study_pace = $request->study_pace;
        $program->education_level = $request->education_level;
        $program->school_free = $request->school_fee;
        $program->start_date = $request->start_date;
        $program->ref_link = $request->ref_link;
        $program->description = $request->description;
        $program->admission = $request->admission;
        $program->language = $request->language;
        $program->content = $request->content;
        $program->qualification = $request->qualification;
        $program->career_path = $request->career;
        $program->root_source = $request->root_web;

        $program->save();

        $program->categories()->sync($request->categories);
        $program->levels()->sync($request->levels);

        return redirect()->route('admin.program.create')->with('messenger', 'Create Success');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Program  $program
     * @return \Illuminate\Http\Response
     */
    // public function show(Program $program)
    // {
    //     //
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Program  $program
     * @return \Illuminate\Http\Response
     */
    public function edit(Program $program)
    {
        $current_user = Auth::user()->id;
        $program_id = $program->user_id;
        if(Gate::allows('access_user_university') || $current_user==$program_id) {
            $current_organization = Auth::user()->relation_organization->id;
            $organizations = Organization::where('id', '=', $current_organization)->get();
            $locations = Location::all();
            $levels = Level::all();
            $categories = Category::all();
            // $current_user = Auth::user()->id;
            // $program = Program::where('user_id', '=', $current_user);

            return view('admin.program.edit')->with([
                'program' => $program,
                'organizations' => $organizations,
                'levels' => $levels,
                'categories' => $categories,
                'locations' => $locations
            ]);

        }
        return redirect()->route('admin.program.index')->with('messenger','Not Access');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Program  $program
     * @return \Illuminate\Http\Response
     */
    public function update(ProgramEditRequest $request, Program $program)
    {
        $program->name = $request->name;
        $program->organization_id = $request->organization;
        $program->location_id = $request->location;
        $program->user_id = Auth::user()->id;
        $program->length = $request->length;
        $program->study_pace = $request->study_pace;
        $program->education_level = $request->education_level;
        $program->school_free = $request->school_fee;
        $program->start_date = $request->start_date;
        $program->ref_link = $request->ref_link;
        $program->description = $request->description;
        $program->admission = $request->admission;
        $program->language = $request->language;
        $program->content = $request->content;
        $program->qualification = $request->qualification;
        $program->career_path = $request->career;
        $program->root_source = $request->root_web;

        $program->save();

        $program->categories()->sync($request->categories);
        $program->levels()->sync($request->levels);

        return redirect()->route('admin.program.index')->with('messenger', 'Update Success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Program  $program
     * @return \Illuminate\Http\Response
     */
    public function destroy(Program $program)
    {
        if(Gate::denies('delete_programs')) {
            return redirect()->route('admin.program.index')->with('messenger','Not Access');
        }
        $program->delete();

        return redirect()->route('admin.program.index')->with('messenger', 'Delete Success');
    }
}
