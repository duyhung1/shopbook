<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Organization;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\OrganizationCreateRequest;
use App\Http\Requests\OrganizationEditRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Auth;

class OrganizationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if(Gate::allows('access_useradmin')) {
            $organizations = Organization::paginate(10);
        }else {
            $current_organization = Auth::user()->organization;
            // dd($current_organization);
            $organizations = Organization::where('id', '=', $current_organization)->get();
        }
        // dd($organizations);
        return view('admin.organization.index', ['organizations' => $organizations]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.organization.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OrganizationCreateRequest $request)
    {
        if($request->hasFile('logo')) {
            $filenameWithExt = $request->file('logo')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('logo')->getClientOriginalExtension();
            $fileNameToStore = $filename."_".time().".".$extension;
            $path = $request->file('logo')->storeAs('public/logos',$fileNameToStore);
        }else {
            $fileNameToStore = 'noimage.jpg';
        }

        $organization = new Organization;
        $organization->name = $request->name;
        $organization->logo = $fileNameToStore;
        $organization->foundation_year = $request->create_at;
        $organization->address = $request->address;
        $organization->description = $request->description;
        $organization->url = $request->url;

        $organization->save();

        return redirect()->route('admin.organization.create')->with('messenger', 'Create Success');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Organization  $organization
     * @return \Illuminate\Http\Response
     */
    public function show(Organization $organization)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Organization  $organization
     * @return \Illuminate\Http\Response
     */
    public function edit(Organization $organization)
    {
        return view('admin.organization.edit', ['organization' =>$organization]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Organization  $organization
     * @return \Illuminate\Http\Response
     */
    public function update(OrganizationEditRequest $request, Organization $organization)
    {
        if($request->hasFile('logo')) {
            $filenameWithExt = $request->file('logo')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('logo')->getClientOriginalExtension();
            $fileNameToStore = $filename."_".time().".".$extension;
            $path = $request->file('logo')->storeAs('public/logos',$fileNameToStore);
        }

        $organization->name = $request->name;
        // unset($organization->name);

        if($request->hasFile('logo')) {
            if($organization->logo != "noimage.jpg"){
                Storage::delete('public/logos/'.$organization->logo);
            }
            $organization->logo = $fileNameToStore;
        }
        $organization->foundation_year = $request->create_at;
        $organization->address = $request->address;
        $organization->description = $request->description;
        $organization->url = $request->url;

        $organization->save();

        return redirect()->route('admin.organization.index')->with('messenger', 'Update Success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Organization  $organization
     * @return \Illuminate\Http\Response
     */
    public function destroy(Organization $organization)
    {
        if(Gate::allows('access_users')) {
            if($organization->logo != "noimage.jpg"){
                Storage::delete('public/logos/'.$organization->logo);
            }
            $organization->delete();
            return redirect()->route('admin.organization.index')->with('messenger', 'Delete Success');
        }else {
            return redirect()->route('admin.organization.index')->with('messenger', 'Delete not Access');
        }

    }
}
