<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProgramEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'organization' => 'required',
            // 'category' => 'required',
            // 'level' => 'required',
            'location' => 'required',
            'length' => 'required',
            'study_pace' => 'required',
            'education_level' => 'required',
            'school_fee' => 'bail|required|numeric',
            'start_date' => 'bail|required|date',
            'ref_link' => 'required',
            'description' => 'required',
            'admission' => 'required',
            'language' => 'required',
            'content' => 'required',
            'qualification'=> 'required',
            'career' => 'required',
            'root_web' => 'required'
        ];
    }
}
