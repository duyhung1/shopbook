<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'thumb' => 'image',
            'ordinal' => 'numeric',
        ];
    }

    public function messages() {
        return [
            'name.required' => 'Không được để trống',

            'thumb.image' => 'Không đúng định dạng ảnh',

            'ordinal'  => 'Phải là số'
        ];
    }
}
