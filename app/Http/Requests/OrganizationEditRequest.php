<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class OrganizationEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'bail',
                'required',
                Rule::unique('organizations')->ignore($this->organization->id, 'id'),
            ],
            'logo' => 'image',
            'create_at' => 'bail|required|date',
            'address' => 'required',
            'description' => 'required',
            'url' => 'url'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Không được để trống',
            'name.unique' => 'Đã tồn tại',

            'logo.image' => 'Không đúng định dạng ảnh',

            'create_at.required' => 'Không được để trống',
            'create_at.date' => 'Không đúng địn dạng date',

            'address.required' => 'Không được để trống',

            'description.required' => 'Không được để trống',

            'url.url' => 'Không đúng định dạng Url'
        ];
    }
}
