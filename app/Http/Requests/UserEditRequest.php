<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'bail',
                'required',
                Rule::unique('users')->ignore($this->user->id, 'id'),
                'max:32',
                'min:4'
            ],
            'first_name' => 'bail|required|max:32|min:2',
            'last_name' => 'bail|required|max:32|min:2',
            'gender' => 'required',
            'email' => [
                'bail',
                'required',
                'email',
                Rule::unique('users')->ignore($this->user->id, 'id'),
                'max:100'
            ],
            'phone' => [
                'bail',
                'required',
                'numeric',
                Rule::unique('users')->ignore($this->user->id, 'id'),
                'min:10',
            ],
            'thumb' => 'image',
            'description' => 'string',
            'organization' => 'required',
            'role' => 'required',
            'active' => 'required',
            'password' => 'bail|nullable|min:8',
            'repasswprd' => 'bail|nullable|same:password'
        ];
    }

    public function messages() {
        return [
            'name.required' => 'Không được để trống',
            'name.unique' => 'Đã tồn tại',
            'name.max' => 'Không được vượt quá 32 ký tự',
            'name.min' => 'Phải có ít nhất 4 ký tự',

            'first_name.required' => 'Không được để trống',
            'first_name.max' => 'Không được vượt quá 32 ký tự',
            'first_name.min' => 'Phải có ít nhất 2 ký tự',

            'last_name.required' => 'Không được để trống',
            'last_name.max' => 'Không được vượt quá 32 ký tự',
            'last_name.min' => 'Phải có ít nhất 2 ký tự',

            'gender.required' => 'Không được để trống',

            'email.required' => 'Không được để trống',
            'email.email' => 'Không đúng định dạng mail',
            'email.unique' => 'Email đã tồn tại',
            'email.max' => 'Không được vượt quá 100 ký tự',

            'phone.required' => 'Không được để trống',
            'phone.numeric' => 'Phải là số',
            'phone.unique' => 'Số điện thoại đã tồn tại',
            'phone.min' => 'Phải có ít nhất 10 ký tự',

            'thumb.image' => 'Không đúng định dạng',

            // 'description' => 'Phải là 1 chuỗi'

            'organization.required' => 'Không được để trống',

            'role.required' => 'Không được để trống',

            'active.required' => 'Không được để trống', 

            'password.required' => 'Không được để trống',
            'password.min' => 'Tối thiểu phải có 8 ký tự',

            'repassword.required' => 'Không được để trống',
            'repassword.same' => 'Mật khẩu nhập lại không đúng'
        ];
    }
}
