<div class="navbar navbar-expand-sm bg-light navbar-light d-flex justify-content-between header">
    <a href="#" class="navbar-brand " ><h2>{{ __('invariable_admin.header.admin')}}</h2></a>
    <div class="d-flex">
        <div class="leguage">
            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
                <!-- <i class="fas fa-user"></i> -->
            </a>
            <div class="dropdown-menu ">
                <a class="dropdown-item" href="{{ route('admin.lang', ['lang' =>'en']) }}"><i class="fas fa-user"></i> EN</a>
                <a class="dropdown-item" href="{{ route('admin.lang', ['lang' =>'vi']) }}"><i class="fas fa-cog"></i> VI</a>
            </div>
        </div>
        <div>
            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
                <!-- <i class="fas fa-user"></i>  -->
                {{  Auth::user()->name }}
            </a>
            <div class="dropdown-menu dropdown-user">
                <!-- <a class="dropdown-item" href="#"><i class="fas fa-user"></i> {{  Auth::user()->name }}</a> -->
                <a class="dropdown-item" href="#"><i class="fas fa-cog"></i> {{ __('invariable_admin.header.setting_user')}}</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="{{ route('logout')}}" onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                    <i class="fas fa-sign-out-alt"></i>
                    {{ __('invariable_admin.header.login_user')}}
                    </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </div>

    </div>

</div>
