<div class="menu">
    <ul class="nav flex-column" id="side-menu">
        <li class="list-menu-bottom">
            <a href="#">
                <i class="fas fa-tachometer-alt"></i>
                {{ __('invariable_admin.menu.dasboard')}}
            </a>
        </li>
        @can('manage-organization')
        <li class="list-menu-bottom user-list">
            <a href="#">
                <i class="fas fa-users"></i>
                {{ __('invariable_admin.menu.user')}}
                <!-- <span class="fas fa-chevron-left arrow"></span> -->
                <span class="fas fa-chevron-down arrow"></span>
            </a>
            <ul class="list-menu-top ">
                <li>
                    <a href="{{ route('admin.users.index')}}">{{ __('invariable_admin.menu.list_user')}}</a>
                </li>
                <li>
                    <a href="{{ route('admin.users.create')}}">{{ __('invariable_admin.menu.add_user')}}</a>
                </li>
            </ul>
        </li>
        <li class="list-menu-bottom organization-list">
            <a href="#" ><i class="fas fa-list-alt"></i>
            {{ __('invariable_admin.menu.organization')}}
                <!-- <span class="fas fa-chevron-left arrow"></span> -->
                <span class="fas fa-chevron-down arrow"></span>
            </a>
            <ul class="list-menu-top ">
                <li>
                    <a href="{{ route('admin.organization.index') }}">{{ __('invariable_admin.menu.list_organization')}}</a>
                </li>
                <li>
                    <a href="{{ route('admin.organization.create') }}">{{ __('invariable_admin.menu.add_organization')}}</a>
                </li>
            </ul>
        </li>
        @endcan
        @can('access_useradmin')
        <li class="list-menu-bottom category-list">
            <a href="#" ><i class="fas fa-list-alt"></i>
            {{ __('invariable_admin.menu.category')}}
                <!-- <span class="fas fa-chevron-left arrow"></span> -->
                <span class="fas fa-chevron-down arrow"></span>
            </a>
            <ul class="list-menu-top ">
                <li>
                    <a href="{{ route('admin.category.index') }}">{{ __('invariable_admin.menu.list_category')}}</a>
                </li>
                <li>
                    <a href="{{ route('admin.category.create') }}">{{ __('invariable_admin.menu.add_category')}}</a>
                </li>
            </ul>
        </li>
        @endcan
        @can('access_useradmin')
        <li class="list-menu-bottom level-list">
            <a href="#" ><i class="fas fa-list-alt"></i>
            {{ __('invariable_admin.menu.level')}}
                <!-- <span class="fas fa-chevron-left arrow"></span> -->
                <span class="fas fa-chevron-down arrow"></span>
            </a>
            <ul class="list-menu-top ">
                <li>
                    <a href="{{ route('admin.level.index') }}">{{ __('invariable_admin.menu.list_level')}}</a>
                </li>
                <li>
                    <a href="{{ route('admin.level.create') }}">{{ __('invariable_admin.menu.add_level')}}</a>
                </li>
            </ul>
        </li>
        @endcan
        <li class="list-menu-bottom program-list">
            <a href="#" ><i class="fas fa-cube"></i>
            {{ __('invariable_admin.menu.program')}}
                <!-- <span class="fas fa-chevron-left arrow"></span> -->
                <span class="fas fa-chevron-down arrow"></span>
            </a>
            <ul class="list-menu-top ">
                <li>
                    <a href="{{ route('admin.program.index')}}">{{ __('invariable_admin.menu.list_program')}}</a>
                </li>
                @can('create_programs')
                <li>
                    <a href="{{ route('admin.program.create')}}">{{ __('invariable_admin.menu.add_program')}}</a>
                </li>
                @endcan
            </ul>
        </li>
        {{-- <li class="list-menu-bottom bill-list">
            <a href="#" ><i class="fas fa-file-invoice-dollar"></i>
            {{ __('invariable_admin.menu.bill')}}
                <!-- <span class="fas fa-chevron-left arrow"></span> -->
                <!-- <span class="fas fa-chevron-down arrow"></span> -->
            </a>
        </li> --}}
        {{-- <li class="list-menu-bottom news-list">
            <a href="#" ><i class="fas fa-newspaper"></i>
            {{ __('invariable_admin.menu.news')}}
                <!-- <span class="fas fa-chevron-left arrow"></span> -->
                <span class="fas fa-chevron-down arrow"></span>
            </a>
            <ul class="list-menu-top ">
                <li>
                    <a href="#">{{ __('invariable_admin.menu.list_news')}}</a>
                </li>
                <li>
                    <a href="#">{{ __('invariable_admin.menu.add_news')}}</a>
                </li>
            </ul>
        </li> --}}
        @can('access_useradmin')
        <li class="list-menu-bottom slide-list">
            <a href="#" ><i class="fas fa-images"></i>
            {{ __('invariable_admin.menu.image')}}
                <!-- <span class="fas fa-chevron-left arrow"></span> -->
                <span class="fas fa-chevron-down arrow"></span>
            </a>
            <ul class="list-menu-top ">
                <li>
                    <a href="{{ route('admin.logo.index')}}">{{ __('invariable_admin.menu.list_image')}}</a>
                </li>
                <li>
                    <a href="{{ route('admin.logo.create')}}">{{ __('invariable_admin.menu.add_image')}}</a>
                </li>
            </ul>
        </li>
        @endcan
    </ul>
</div>
