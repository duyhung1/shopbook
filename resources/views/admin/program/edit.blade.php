@extends('admin.layouts_admin.index')

@section('content')
{{-- <div class="container"> --}}
    @if(session('messenger'))
        <div class="alert alert-success" role="alert">
            {{session('messenger')}}
        </div>
    @endif
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header d-flex">
                    <h4>Edit Program: {{$program->name}}</h4>
                    <a href="{{ route('admin.program.index') }}" class="ml-auto">Back</a>
                </div>
                <div class="card-body">
                    <form action="{{ route('admin.program.update', $program) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    {{method_field('PUT')}}
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="Please Enter Username" value="{{ $program->name }}">
                            @if($errors->has('name'))
                                <small id="emailHelp" class="form-text text-muted">{{$errors->first('name')}}</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Organization</label>
                            <select name="organization" id="organization" class="custom-select">
                                <option selected value="">Choose...</option>
                                @foreach($organizations as $organization)
                                <option value="{{$organization->id}}">{{$organization->name}}</option>
                                @endforeach
                            </select>
                            @if($errors->has('organization'))
                                <small id="emailHelp" class="form-text text-muted">{{$errors->first('organization')}}</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Category</label>
                            @foreach ( $categories as $category )
                                <input type="checkbox" name="categories[]" id="" value="{{ $category->id }}">
                                <label>{{ $category->name }}</label>
                            @endforeach
                            @if($errors->has('category'))
                                <small id="emailHelp" class="form-text text-muted">{{$errors->first('category')}}</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Level</label>
                            @foreach ( $levels as $level )
                                <input type="checkbox" name="levels[]" id="" value="{{ $level->id }}">
                                <label>{{ $level->name }}</label>
                            @endforeach
                            @if($errors->has('level'))
                                <small id="emailHelp" class="form-text text-muted">{{$errors->first('level')}}</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Location</label>
                            <select name="location" id="" class="custom-select">
                                <option selected value="">Choose...</option>
                                @foreach($locations as $location)
                                <option value="{{$location->id}}">{{$location->name}}</option>
                                @endforeach
                            </select>
                            @if($errors->has('location'))
                                <small id="emailHelp" class="form-text text-muted">{{$errors->first('location')}}</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Length</label>
                            <input class="no_drop form-control" type="text" name="length" id="" placeholder="Please Enter Training Time" value="{{ $program->length }}" >
                            @if($errors->has('length'))
                                <small id="emailHelp" class="form-text text-muted">{{$errors->first('length')}}</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Study Pace</label>
                            <input type="text" class="form-control" name="study_pace" id="" placeholder="Please Enter Study Pace" value="{{ $program->study_pace }}">
                            @if($errors->has('study_pace'))
                                <small id="emailHelp" class="form-text text-muted">{{$errors->first('study_pace')}}</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Education Level</label>
                            <input type="text" id="" name="education_level"  value="{{ $program->education_level }}" class="form-control" placeholder="Please Enter Education Level">
                            @if($errors->has('education_level'))
                                <small  class="form-text text-muted">{{$errors->first('education_level')}}</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>School Fee</label>
                            <input type="text" id="" name="school_fee"  value="{{ $program->school_free }}" class="form-control" placeholder="Please Enter School Fee">
                            @if($errors->has('school_fee'))
                                <small  class="form-text text-muted">{{$errors->first('school_fee')}}</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Start date</label>
                            <input type="date" id="" name="start_date"  value="{{ $program->start_date }}" class="form-control">
                            @if($errors->has('start_date'))
                                <small  class="form-text text-muted">{{$errors->first('start_date')}}</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Reference Link</label>
                            <input type="text" id="" name="ref_link"  value="{{ $program->ref_link }}" class="form-control" placeholder="Please Enter Reference Link">
                            @if($errors->has('ref_link'))
                                <small  class="form-text text-muted">{{$errors->first('ref_link')}}</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Description</label>
                            <textarea name="description" id="" cols="22" rows="3" class="form-control" >{{ $program->description }}</textarea>
                            @if($errors->has('description'))
                                <small id="emailHelp" class="form-text text-muted">{{$errors->first('description')}}</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Admission Requirements</label>
                            <textarea name="admission" id="" cols="22" rows="3" class="form-control" >{{ $program->admission }}</textarea>
                            @if($errors->has('admission'))
                                <small id="emailHelp" class="form-text text-muted">{{$errors->first('admission')}}</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Language Requirements</label>
                            <textarea name="language" id="" cols="22" rows="3" class="form-control" >{{ $program->language }}</textarea>
                            @if($errors->has('language'))
                                <small id="emailHelp" class="form-text text-muted">{{$errors->first('language')}}</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Content</label>
                            <textarea name="content" id="" cols="22" rows="5" class="form-control" >{{ $program->content }}</textarea>
                            @if($errors->has('content'))
                                <small id="emailHelp" class="form-text text-muted">{{$errors->first('content')}}</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Qualification</label>
                            <input type="text" id="" name="qualification"  value="{{ $program->qualification }}" class="form-control" placeholder="Please Enter Qualification">
                            @if($errors->has('qualification'))
                                <small  class="form-text text-muted">{{$errors->first('qualification')}}</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Career Opportunities</label>
                            <input type="text" id="" name="career"  value="{{ $program->career_path }}" class="form-control" placeholder="Please Enter Career Opportunities">
                            @if($errors->has('career'))
                                <small  class="form-text text-muted">{{$errors->first('career')}}</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Root Web of Program</label>
                            <input type="text" id="" name="root_web"  value="{{ $program->root_source }}" class="form-control" placeholder="Please Enter Root Web of Program">
                            @if($errors->has('root_web'))
                                <small  class="form-text text-muted">{{$errors->first('root_web')}}</small>
                            @endif
                        </div>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
{{-- </div> --}}
@endsection

