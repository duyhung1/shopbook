@extends('admin.layouts_admin.index')

@section('content')
{{-- <div class="container-fluid"> --}}
    @if(session('messenger'))
        <div class="alert alert-success" role="alert">
            {{session('messenger')}}
        </div>
    @endif
    <!-- <div class="row justify-content-center">
        <div class="col-md-8"> -->
            <div class="card">
                <div class="card-header d-flex">
                    <h4>Program</h4>
                    {{-- @can('create_programs')
                    <a href="{{ route('admin.program.create') }}" class="ml-auto"><button type="button" class="btn btn-success">Create Program</button></a>
                    @endcan --}}
                </div>


                <div class="card-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col">Organization</th>
                            <th scope="col">Category</th>
                            <th scope="col">Level</th>
                            <th scope="col">Location</th>
                            <th scope="col">Length</th>
                            <th scope="col">Study Pace</th>
                            <th scope="col">Education Level</th>
                            <th scope="col">School Free</th>
                            <th scope="col">Start Date</th>
                            <th scope="col">Ref Link</th>
                            <th scope="col">Description</th>
                            <th scope="col">Admission</th>
                            <th scope="col">Language</th>
                            <th scope="col">Qualification</th>
                            <th scope="col">Career Path</th>
                            <th scope="col">Root Source</th>
                            <th scope="col">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($programs as $program)
                        <tr>
                            <th scope="row">{{$program->id}}</th>
                            <td>{{ $program->name }}</td>
                            <td>{{ $program->organizations->name }}</td>
                            <td>{{ implode(", ",$program->categories()->get()->pluck('name')->toArray()) }}</td>
                            <td>{{ implode(", ",$program->levels()->get()->pluck('name')->toArray()) }}</td>
                            <td>{{ $program->locations->name }}</td>
                            <td>{{ $program->length }}</td>
                            <td>{{ $program->study_pace }}</td>
                            <td>{{ $program->education_level }}</td>
                            <td>{{ $program->school_free }}</td>
                            <td>{{ $program->start_date }}</td>
                            <td>{{ $program->ref_link }}</td>
                            <td>{{ $program->description }}</td>
                            <td>{{ $program->admission }}</td>
                            <td>{{ $program->language }}</td>
                            <td>{{ $program->content }}</td>
                            <td>{{ $program->qualification }}</td>
                            <td>{{ $program->career_path }}</td>
                            <td>{{ $program->root_source }}</td>
                            <td class="d-flex">
                                @can('edit_programs')
                                <a href="{{ route('admin.program.edit', $program->id) }}"><button type="button" class="btn btn-primary">Edit</button></a>
                                @endcan
                                @can('delete_programs')
                                <form action="{{ route('admin.program.destroy', $program) }}" method="post">
                                @csrf
                                    {{method_field('DELETE')}}
                                    <button type="submit" class="btn btn-warning">Delete</button>
                                </form>
                                @endcan
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{$programs->links()}}
                </div>
            </div>
        <!-- </div>
    </div> -->
{{-- </div> --}}
<script>
    $(document).ready(function(){
        $("li.program-list >ul:last").slideDown();
        $("li.program-list >ul:last li:first").addClass("active");
    });
</script>
@endsection
