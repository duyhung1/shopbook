@extends('admin.layouts_admin.index')

@section('content')
{{-- <div class="container"> --}}
    @if(session('messenger'))
        <div class="alert alert-success" role="alert">
            {{session('messenger')}}
        </div>
    @endif
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header d-flex">
                    <h4>Edit Category: {{$category->name}}</h4>
                    <a href="{{ route('admin.category.index') }}" class="ml-auto">Back</a>
                </div>
                <div class="card-body">
                    <form action="{{ route('admin.category.update', $category) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    {{method_field('PUT')}}
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="Please Enter Name" value="{{ $category->name }}">
                            @if($errors->has('name'))
                                <small class="form-text text-muted">{{$errors->first('name')}}</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Thumb</label>
                            <input type="file" id="" name="thumb"  value="" class="form-control">
                            @if($errors->has('thumb'))
                                <small  class="form-text text-muted">{{$errors->first('thumb')}}</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Ordinal</label>
                            <input class="form-control" type="number" name="ordinal" id=""  value="{{ $category->ordinal }}" >
                            @if($errors->has('ordinal'))
                                <small  class="form-text text-muted">{{$errors->first('ordinal')}}</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Description</label>
                            <textarea name="description" id="des" cols="22" rows="3" class="form-control" >{{ $category->description }}</textarea>
                            @if($errors->has('description'))
                                <small class="form-text text-muted">{{$errors->first('description')}}</small>
                            @endif
                        </div>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
{{-- </div> --}}
@endsection
