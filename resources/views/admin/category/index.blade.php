@extends('admin.layouts_admin.index')

@section('content')
{{-- <div class="container"> --}}
    @if(session('messenger'))
        <div class="alert alert-success" role="alert">
            {{session('messenger')}}
        </div>
    @endif
    <!-- <div class="row justify-content-center"> -->
        <!-- <div class="col-md-8"> -->
            <div class="card">
                <div class="card-header d-flex">
                    <h4>Category</h4>
                    {{-- @can('access_users')
                    <a href="{{ route('admin.category.create') }}" class="ml-auto"><button type="button" class="btn btn-success">Create Category</button></a>
                    @endcan --}}
                </div>


                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Name</th>
                                <th scope="col">Thumb</th>
                                <th scope="col">Ordinal</th>
                                <th scope="col">Description</th>
                                <th scope="col">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($categories as $category)
                            <tr>
                                <th scope="row">{{$category->id}}</th>
                                <td>{{ $category->name }}</td>
                                <td><img style="width:100%;" src="http://127.0.0.1/boookshop/public/storage/category_thumbs/{{$category->thumb}}" alt=""></td>
                                <td>{{ $category->ordinal }}</td>
                                <td>{{ $category->description }}</td>

                                <td class="d-flex">
                                    <a href="{{ route('admin.category.edit', $category->id) }}"><button type="button" class="btn btn-primary">Edit</button></a>
                                    <form action="{{ route('admin.category.destroy', $category) }}" method="post">
                                    @csrf
                                    {{method_field('DELETE')}}
                                    <button type="submit" class="btn btn-warning">Delete</button>
                                </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{$categories->links()}}
                </div>
            </div>
        <!-- </div> -->
    <!-- </div> -->
{{-- </div> --}}
<script>
    $(document).ready(function(){
        $("li.category-list >ul:last").slideDown();
        $("li.category-list >ul:last li:first").addClass("active");
    });
</script>
@endsection
