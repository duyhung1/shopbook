@extends('layouts.app')

@section('content')
<div class="container">
    @if(session('messenger'))
        <div class="alert alert-success" role="alert">
            {{session('messenger')}}
        </div>
    @endif
    <!-- <div class="row justify-content-center">
        <div class="col-md-8"> -->
            <div class="card">
                <div class="card-header d-flex">
                    <h4>User</h4>
                    <a href="{{ route('admin.users.create') }}" class="ml-auto"><button type="button" class="btn btn-success">Create User Manager</button></a>
                </div>


                <div class="card-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col">Email</th>
                            <th scope="col">Role</th>
                            <th scope="col">First Name</th>
                            <th scope="col">Last Name</th>
                            <th scope="col">Gender</th>
                            <th scope="col">Phone Number</th>
                            <th scope="col">Thumb</th>
                            <th scope="col">Description</th>
                            <th scope="col">Organization</th>
                            <th scope="col">Active</th>
                            <th scope="col">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $user)
                        <tr>
                            <th scope="row">{{$user->id}}</th>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ implode($user->roles()->get()->pluck('name')->toArray()) }}</td>
                            <td>{{ $user->first_name }}</td>
                            <td>{{ $user->last_name }}</td>
                            <td>{{ $user->gender }}</td>
                            <td>{{ $user->phone }}</td>
                            <td><img style="width:100%;" src="http://127.0.0.1/boookshop/public/storage/thumbs/{{$user->thumb}}" alt=""></td>
                            <td>{{ $user->description }}</td>
                            <td>{{ implode($user->organization()->get()->pluck('name')->toArray()) }}</td>
                            <td>{{ $user->active }}</td>
                            <td class="d-flex">
                                <a href="{{ route('admin.users.edit', $user->id) }}"><button type="button" class="btn btn-primary">Edit</button></a>
                               <form action="{{ route('admin.users.destroy', $user) }}" method="post">
                                   @csrf
                                   {{method_field('DELETE')}}
                                   <button type="submit" class="btn btn-warning">Delete</button>
                               </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{$users->links()}}
                </div>
            </div>
        <!-- </div>
    </div> -->
</div>
@endsection
