@extends('admin.layouts_admin.index')

@section('content')
{{-- <div class="container"> --}}
    @if(session('messenger'))
        <div class="alert alert-success" role="alert">
            {{session('messenger')}}
        </div>
    @endif
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header d-flex">
                    <h4>Edit User: {{$user->name}}</h4>
                    <a href="{{ route('admin.users.index') }}" class="ml-auto">Back</a>
                </div>
                <div class="card-body">
                    <form action="{{ route('admin.users.update', $user) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    {{method_field('PUT')}}
                        <div class="form-group">
                            <label>Username</label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="Please Enter Username" value="{{ $user->name }}">
                            @if($errors->has('name'))
                                <small id="emailHelp" class="form-text text-muted">{{$errors->first('name')}}</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>First Name</label>
                            <input type="text" class="form-control" name="first_name" id="" placeholder="Please Enter First Name" value="{{ $user->first_name }}">
                            @if($errors->has('first_name'))
                                <small id="emailHelp" class="form-text text-muted">{{$errors->first('first_name')}}</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Last Name</label>
                            <input type="text" class="form-control" name="last_name" id="" placeholder="Please Enter Last Name" value="{{ $user->last_name }}">
                            @if($errors->has('last_name'))
                                <small id="emailHelp" class="form-text text-muted">{{$errors->first('last_name')}}</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Gender: </label>
                            <input type="radio" class="" name="gender" id="" value="male" @if($user->gender == 'male') checked @endif>
                            <label>Male</label>
                            <input type="radio" class="" name="gender" id="" value="female"@if($user->gender == 'female') checked @endif>
                            <label>Female</label>
                            <input type="radio" class="" name="gender" id="" value="other"@if($user->gender == 'other') checked @endif>
                            <label>Other</label>
                            @if($errors->has('gender'))
                                <small id="emailHelp" class="form-text text-muted">{{$errors->first('gender')}}</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input class="no_drop form-control" type="email" name="email" id="" placeholder="Please Enter Email" value="{{ $user->email }}" >
                            @if($errors->has('email'))
                                <small id="emailHelp" class="form-text text-muted">{{$errors->first('email')}}</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Phone Number</label>
                            <input type="text" class="form-control" name="phone" id="" placeholder="Please Enter Phone Number" value="{{ $user->phone }}">
                            @if($errors->has('phone'))
                                <small id="emailHelp" class="form-text text-muted">{{$errors->first('phone')}}</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Thumb</label>
                            <input type="file" id="photo" name="thumb"  value="" class="form-control">
                            @if($errors->has('thumb'))
                                <small  class="form-text text-muted">{{$errors->first('thumb')}}</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Description</label>
                            <textarea name="description" id="des" cols="22" rows="3" class="form-control" >{{ $user->description }}</textarea>
                            @if($errors->has('description'))
                                <small id="emailHelp" class="form-text text-muted">{{$errors->first('description')}}</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Organization</label>
                            <select name="organization" id="organization" class="custom-select">
                                <option selected>Choose...</option>
                                @foreach($organizations as $organization)
                                <option value="{{$organization->id}}">{{$organization->name}}</option>
                                @endforeach
                            </select>
                            @if($errors->has('organization'))
                                <small id="emailHelp" class="form-text text-muted">{{$errors->first('organization')}}</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Active: </label>
                            <input type="radio" class="" name="active" id="" value="yes" @if($user->active == 'yes') checked @endif>
                            <label>Yes</label>
                            <input type="radio" class="" name="active" id="" value="no" @if($user->active == 'no') checked @endif>
                            <label>No</label>
                            @if($errors->has('active'))
                                <small id="emailHelp" class="form-text text-muted">{{$errors->first('active')}}</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Role: </label>
                            @foreach($roles as $role)
                            <input type="radio" class="" name="role[]" id="" value="{{$role->id}}">
                            <label>{{$role->name}}</label>
                            @endforeach
                            @if($errors->has('role'))
                                <small id="emailHelp" class="form-text text-muted">{{$errors->first('role')}}</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" class="form-control" name="password" id="" placeholder="Please Enter Password" value="">
                            @if($errors->has('password'))
                                <small id="emailHelp" class="form-text text-muted">{{$errors->first('password')}}</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>RePassword</label>
                            <input type="password" class="form-control" name="repassword" id="" placeholder="Please Enter RePassword">
                            @if($errors->has('repasswprd'))
                                <small id="emailHelp" class="form-text text-muted">{{$errors->first('repasswprd')}}</small>
                            @endif
                        </div>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
{{-- </div> --}}
@endsection

