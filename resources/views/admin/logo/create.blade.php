@extends('admin.layouts_admin.index')

@section('content')
<div class="container">
    @if(session('messenger'))
        <div class="alert alert-success" role="alert">
            {{session('messenger')}}
        </div>
    @endif
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header d-flex">
                    <h4>Create Logo</h4>
                    <a href="{{ route('admin.logo.index') }}" class="ml-auto">Back</a>
                </div>
                <div class="card-body">
                    <form action="{{ route('admin.logo.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="Please Enter Username" value="{{ old('name') }}">
                            @if($errors->has('name'))
                                <small class="form-text text-muted">{{$errors->first('name')}}</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Logo</label>
                            <input type="file" id="photo" name="logo"  value="" class="form-control">
                            @if($errors->has('logo'))
                                <small  class="form-text text-muted">{{$errors->first('logo')}}</small>
                            @endif
                        </div>
                        <button type="submit" class="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $("li.slide-list >ul:last").slideDown();
        $("li.slide-list >ul:last li:last").addClass("active");
    });
</script>
@endsection
