@extends('admin.layouts_admin.index')

@section('content')
<div class="container">
    @if(session('messenger'))
        <div class="alert alert-success" role="alert">
            {{session('messenger')}}
        </div>
    @endif
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header d-flex">
                    <h4>Edit Organization: {{$organization->name}}</h4>
                    <a href="{{ route('admin.organization.index') }}" class="ml-auto">Back</a>
                </div>
                <div class="card-body">
                    <form action="{{ route('admin.organization.update', $organization) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    {{method_field('PUT')}}
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="Please Enter Username" value="{{ $organization->name }}"
                            @if(Gate::denies('access_users')) readonly @endif>
                            @if($errors->has('name'))
                                <small class="form-text text-muted">{{$errors->first('name')}}</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Logo</label>
                            <input type="file" id="photo" name="logo"  value="" class="form-control">
                            @if($errors->has('logo'))
                                <small  class="form-text text-muted">{{$errors->first('logo')}}</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Foundation Year</label>
                            <input class="form-control" type="date" name="create_at" id=""  value="{{ $organization->foundation_year }}" >
                            @if($errors->has('create_at'))
                                <small  class="form-text text-muted">{{$errors->first('create_at')}}</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Address</label>
                            <input type="text" class="form-control" name="address" id="" placeholder="Please Enter Address" value="{{ $organization->address }}">
                            @if($errors->has('address'))
                                <small  class="form-text text-muted">{{$errors->first('address')}}</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Description</label>
                            <textarea name="description" id="des" cols="22" rows="3" class="form-control" >{{ $organization->description }}</textarea>
                            @if($errors->has('description'))
                                <small class="form-text text-muted">{{$errors->first('description')}}</small>
                            @endif
                        </div>
                        <div class="from-group">
                            <label>Ulr</label>
                            <input type="text" class="form-control" name="url" id="" placeholder="Please Enter Ulr" value="{{ $organization->url }}">
                            @if($errors->has('url'))
                                <small  class="form-text text-muted">{{$errors->first('url')}}</small>
                            @endif
                        </div>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
