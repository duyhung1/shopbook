@extends('admin.layouts_admin.index')

@section('content')
<div class="container">
    @if(session('messenger'))
        <div class="alert alert-success" role="alert">
            {{session('messenger')}}
        </div>
    @endif
    <!-- <div class="row justify-content-center"> -->
        <!-- <div class="col-md-8"> -->
            <div class="card">
                <div class="card-header d-flex">
                    <h4>Logo</h4>
                    {{-- @can('access_users') --}}
                    <a href="{{ route('admin.logo.create') }}" class="ml-auto"><button type="button" class="btn btn-success">Create Logo</button></a>
                    {{-- @endcan --}}
                </div>


                <div class="card-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col">Logo</th>
                            <th scope="col">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($logos as $logo)
                        <tr>
                            <th scope="row">{{$logo->id}}</th>
                            <td>{{ $logo->name }}</td>
                            <td><img style="width:100%;" src="http://127.0.0.1/boookshop/public/storage/logomaster/{{$logo->logo}}" alt=""></td>
                            <td class="d-flex">
                                <a href="{{ route('admin.logo.edit', $logo->id) }}"><button type="button" class="btn btn-primary">Edit</button></a>
                                {{-- @can('access_users') --}}
                                <form action="{{ route('admin.logo.destroy', $logo) }}" method="post">
                                @csrf
                                    {{method_field('DELETE')}}
                                    <button type="submit" class="btn btn-warning">Delete</button>
                                </form>
                                {{-- @endcan --}}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                @can('access_users')
                {{$logos->links()}}
                @endcan
                </div>
            </div>
        <!-- </div> -->
    <!-- </div> -->
</div>
<script>
    $(document).ready(function(){
        $("li.slide-list >ul:last").slideDown();
        $("li.slide-list >ul:last li:first").addClass("active");
    });
</script>
@endsection
