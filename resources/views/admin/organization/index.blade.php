@extends('admin.layouts_admin.index')

@section('content')
{{-- <div class="container"> --}}
    @if(session('messenger'))
        <div class="alert alert-success" role="alert">
            {{session('messenger')}}
        </div>
    @endif
    <!-- <div class="row justify-content-center"> -->
        <!-- <div class="col-md-8"> -->
            <div class="card">
                <div class="card-header d-flex">
                    <h4>Organization</h4>
                    {{-- @can('access_useradmin')
                    <a href="{{ route('admin.organization.create') }}" class="ml-auto"><button type="button" class="btn btn-success">Create Crganization</button></a>
                    @endcan --}}
                </div>


                <div class="card-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col">Logo</th>
                            <th scope="col">Foundation Year</th>
                            <th scope="col">Address</th>
                            <th scope="col">Description</th>
                            <th scope="col">Ulr</th>
                            <th scope="col">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($organizations as $organization)
                        <tr>
                            <th scope="row">{{$organization->id}}</th>
                            <td>{{ $organization->name }}</td>
                            <td><img style="width:100%;" src="http://127.0.0.1/boookshop/public/storage/logos/{{$organization->logo}}" alt=""></td>
                            <td>{{ $organization->foundation_year }}</td>
                            <td>{{ $organization->address }}</td>
                            <td>{{ $organization->description }}</td>
                            <td>{{ $organization->url }}</td>

                            <td class="d-flex">
                                <a href="{{ route('admin.organization.edit', $organization->id) }}"><button type="button" class="btn btn-primary">Edit</button></a>
                                @can('access_useradmin')
                                <form action="{{ route('admin.organization.destroy', $organization) }}" method="post">
                                @csrf
                                    {{method_field('DELETE')}}
                                    <button type="submit" class="btn btn-warning">Delete</button>
                                </form>
                                @endcan
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                @can('access_users')
                {{$organizations->links()}}
                @endcan
                </div>
            </div>
        <!-- </div> -->
    <!-- </div> -->
{{-- </div> --}}
<script>
    $(document).ready(function(){
        $("li.organization-list >ul:last").slideDown();
        $("li.organization-list >ul:last li:first").addClass("active");
    });
</script>
@endsection
