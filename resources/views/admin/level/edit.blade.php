@extends('admin.layouts_admin.index')

@section('content')
{{-- <div class="container"> --}}
    @if(session('messenger'))
        <div class="alert alert-success" role="alert">
            {{session('messenger')}}
        </div>
    @endif
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header d-flex">
                    <h4>Edit Level: {{$level->name}}</h4>
                    <a href="{{ route('admin.level.index') }}" class="ml-auto">Back</a>
                </div>
                <div class="card-body">
                    <form action="{{ route('admin.level.update', $level) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    {{method_field('PUT')}}
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="Please Enter Name" value="{{ $level->name }}">
                            @if($errors->has('name'))
                                <small class="form-text text-muted">{{$errors->first('name')}}</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Description</label>
                            <textarea name="description" id="des" cols="22" rows="3" class="form-control" >{{ $level->description }}</textarea>
                            @if($errors->has('description'))
                                <small class="form-text text-muted">{{$errors->first('description')}}</small>
                            @endif
                        </div>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
{{-- </div> --}}
@endsection
