@extends('admin.layouts_admin.index')

@section('content')
{{-- <div class="container"> --}}
    @if(session('messenger'))
        <div class="alert alert-success" role="alert">
            {{session('messenger')}}
        </div>
    @endif
    <!-- <div class="row justify-content-center"> -->
        <!-- <div class="col-md-8"> -->
            <div class="card">
                <div class="card-header d-flex">
                    <h4>Level</h4>
                    @can('access_users')
                    <a href="{{ route('admin.level.create') }}" class="ml-auto"><button type="button" class="btn btn-success">Create Level</button></a>
                    @endcan
                </div>


                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Name</th>
                                <th scope="col">Description</th>
                                <th scope="col">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($levels as $level)
                            <tr>
                                <th scope="row">{{$level->id}}</th>
                                <td>{{ $level->name }}</td>
                                <td>{{ $level->description }}</td>

                                <td class="d-flex">
                                    <a href="{{ route('admin.level.edit', $level->id) }}"><button type="button" class="btn btn-primary">Edit</button></a>
                                    <form action="{{ route('admin.level.destroy', $level) }}" method="post">
                                    @csrf
                                    {{method_field('DELETE')}}
                                    <button type="submit" class="btn btn-warning">Delete</button>
                                </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{$levels->links()}}
                </div>
            </div>
        <!-- </div> -->
    <!-- </div> -->
{{-- </div> --}}
<script>
    $(document).ready(function(){
        $("li.level-list >ul:last").slideDown();
        $("li.level-list >ul:last li:first").addClass("active");
    });
</script>
@endsection
