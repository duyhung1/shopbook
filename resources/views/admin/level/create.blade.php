@extends('admin.layouts_admin.index')

@section('content')
{{-- <div class="container"> --}}
    @if(session('messenger'))
        <div class="alert alert-success" role="alert">
            {{session('messenger')}}
        </div>
    @endif
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header d-flex">
                    <h4>Create Level</h4>
                    <a href="{{ route('admin.level.index') }}" class="ml-auto">Back</a>
                </div>
                <div class="card-body">
                    <form action="{{ route('admin.level.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="Please Enter Username" value="{{ old('name') }}">
                            @if($errors->has('name'))
                                <small class="form-text text-muted">{{$errors->first('name')}}</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Description</label>
                            <textarea name="description" id="des" cols="22" rows="3" class="form-control" >{{ old('description') }}</textarea>
                            @if($errors->has('description'))
                                <small class="form-text text-muted">{{$errors->first('description')}}</small>
                            @endif
                        </div>
                        <button type="submit" class="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
{{-- </div> --}}
<script>
    $(document).ready(function(){
        $("li.level-list >ul:last").slideDown();
        $("li.level-list >ul:last li:last").addClass("active");
    });
</script>
@endsection
