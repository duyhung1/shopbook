@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header d-flex">
                    <a href="">Admin</a>
                    <div class="ml-auto">
                        @can('manage-organization')
                        <a href="{{ route('admin.users.index') }}">Users</a>
                        @endcan
                        <a href="{{ route('admin.organization.index') }}">Organizations</a>
                        @can('access_useradmin')
                        <a href="{{ route('admin.category.index') }}">Categories</a>
                        <a href="{{ route('admin.level.index') }}">Levels</a>
                        @endcan
                        <a href="{{ route('admin.program.index') }}">Programs</a>
                        <a href="{{ route('admin.logo.index') }}">Logos</a>
                    </div>
                </div>

                <div class="card-body">

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
