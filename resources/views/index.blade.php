@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row no-gutters top-main">
        <div class="col-sm-3">
            <ul class="list-group">
                <li class="list-group-item"><a href="" class=" d-flex justify-content-between align-items-center"><span>Sách thường thức-Đời sông</span> <i class="fas fa-chevron-right"></i></a></li>
            </ul>
        </div>
        <div class="col-sm-9">
            <div class="row no-gutters">
                <div class="col-sm-8 slide-01">
                    <a href="">
                        <img src="" alt="">
                    </a>
                </div>
                <div class="col-sm-4 slide-02">
                    <div class=" slide-03">
                        3
                    </div>
                    <div class=" slide-04">
                        2
                    </div>
                </div>
            </div>
            <div class="row no-gutters">
                <div class="col-sm-4 slide-03">
                    5
                </div>
                <div class="col-sm-4 slide-02">
                    5
                </div>
                <div class="col-sm-4 slide-03">
                    5
                </div>
            </div>
        </div>
    </div>
    <div class="row no-gutters main">
        <div class="col-sm-9">
            <h2>Đặt Trước Ngay</h2>
            <div class="row no-gutters datngay">
                <div class="col-sm-4 slide-03">
                    <div class="image-index">
                        a
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="description">
                        <h2 class="name">Tên Sách</h2>
                        <span>Tác giả</span>
                        <p>Mô tả sách</p>
                        <ul class="list-group">
                            <li>2-%</li>
                            <li>2-%</li>
                            <li>2-%</li>
                        </ul>
                    </div>
                </div>
            </div>
            <h2>Sách Bán Chạy</h2>
            <div class="row no-gutters top-banchay">
                <div class="col-sm-4 banchay slide-02">
                    1
                </div>
                <div class="col-sm-4 banchay ">
                    1
                </div>
                <div class="col-sm-4 banchay slide-02">
                    1
                </div>
            </div>
            <div class="row no-gutters top-banchay">
                <div class="col-sm-4 banchay ">
                    1
                </div>
                <div class="col-sm-4 banchay slide-02">
                    1
                </div>
                <div class="col-sm-4 banchay ">
                    1
                </div>
            </div>
            <h2>Sách Hot</h2>
            <div class="row no-gutters datngay">
                4
            </div>
            <h2>Sách Văn Học Mới</h2>
            <div class="row no-gutters top-banchay">
                <div class="col-sm-4 banchay slide-02">
                    1
                </div>
                <div class="col-sm-4 banchay ">
                    1
                </div>
                <div class="col-sm-4 banchay slide-02">
                    1
                </div>
            </div>
            <div class="row no-gutters top-banchay">
                <div class="col-sm-4 banchay ">
                    1
                </div>
                <div class="col-sm-4 banchay slide-02">
                    1
                </div>
                <div class="col-sm-4 banchay ">
                    1
                </div>
            </div>
            <h2>Forbes Tháng 6.2020</h2>
            <div class="row no-gutters datngay">
                4
            </div>
            <h2>Sách Kinh Tế Mới</h2>
            <div class="row no-gutters top-banchay">
                <div class="col-sm-4 banchay slide-02">
                    1
                </div>
                <div class="col-sm-4 banchay ">
                    1
                </div>
                <div class="col-sm-4 banchay slide-02">
                    1
                </div>
            </div>
            <div class="row no-gutters top-banchay">
                <div class="col-sm-4 banchay ">
                    1
                </div>
                <div class="col-sm-4 banchay slide-02">
                    1
                </div>
                <div class="col-sm-4 banchay ">
                    1
                </div>
            </div>
            <h2>Tạp Chí Bán Chạy Nhất</h2>
            <div class="row no-gutters top-banchay">
                <div class="col-sm-4 banchay slide-02">
                    1
                </div>
                <div class="col-sm-4 banchay ">
                    1
                </div>
                <div class="col-sm-4 banchay slide-02">
                    1
                </div>
            </div>
            <div class="row no-gutters top-banchay">
                <div class="col-sm-4 banchay ">
                    1
                </div>
                <div class="col-sm-4 banchay slide-02">
                    1
                </div>
                <div class="col-sm-4 banchay ">
                    1
                </div>
            </div>
            <h2>Tác Giả Nổi Bật</h2>
            <div class="row no-gutters datngay">
                4
            </div>
            <h2>Sách Nuôi Dạy Trẻ</h2>
            <div class="row no-gutters top-banchay">
                <div class="col-sm-4 banchay slide-02">
                    1
                </div>
                <div class="col-sm-4 banchay ">
                    1
                </div>
                <div class="col-sm-4 banchay slide-02">
                    1
                </div>
            </div>
            <div class="row no-gutters top-banchay">
                <div class="col-sm-4 banchay ">
                    1
                </div>
                <div class="col-sm-4 banchay slide-02">
                    1
                </div>
                <div class="col-sm-4 banchay ">
                    1
                </div>
            </div>
            <h2>Sách Thiếu Nhi Mới</h2>
            <div class="row no-gutters top-banchay">
                <div class="col-sm-4 banchay slide-02">
                    1
                </div>
                <div class="col-sm-4 banchay ">
                    1
                </div>
                <div class="col-sm-4 banchay slide-02">
                    1
                </div>
            </div>
            <div class="row no-gutters top-banchay">
                <div class="col-sm-4 banchay ">
                    1
                </div>
                <div class="col-sm-4 banchay slide-02">
                    1
                </div>
                <div class="col-sm-4 banchay ">
                    1
                </div>
            </div>
        </div>
        <div class="col-sm-3 slide-03">
            5
        </div>

    </div>
</div>
@endsection
