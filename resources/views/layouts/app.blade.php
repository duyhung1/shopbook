<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'BookShop') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/header.css') }}" rel="stylesheet">
    <link href="{{ asset('css/index.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
    <!-- top fist -->
        <div class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container ">
                <ul class="navbar-nav mr-auto top-left">
                    <li><span><i class="fas fa-truck-moving"></i> Miễn phí giao hàng</span></li>
                    <div class="gach"></div>
                    <li><span><i class="fas fa-book"></i> 80.000 tựa sách</span></li>
                    <div class="gach"></div>
                    <li><a href=""><span><i class="fas fa-mobile-alt"></i> App Reader</span></a></li>
                </ul>
                <ul class="navbar-nav ml-auto top-right">
                    <li><a class="btn btn-primary" href="#" ><span><i class="fas fa-thumbs-up"></i> Thích 10,72K</span></a></li>
                    <li><a class="btn btn-primary" href="#" role="button"><span>Chia sẻ</span></a></li>
                </ul>
            </div>
        </div>
        <!-- Herder -->
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    BookShop
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <div class="navbar-nav ml-auto">
                        <div class="search navbar-light bg-light">
                            <form class="form-inline">
                                <i class="fas fa-search" aria-hidden="true"></i>
                                <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Tìm sách</button>
                            </form>
                        </div>
                    </div>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item card">
                            <a href="" class="nav-link"><i class="fas fa-shopping-cart"></i></a>
                        </li>

                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">Đăng nhập</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">Đăng ký</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                        Đăng xuất
                                    </a>
                                    <a href="{{ route('admin.index') }}" class="dropdown-item">User Management</a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
        <div class="navbar navbar-expand-lg menu-top">
            <div class="container">
                <div class="mr-auto">
                    <a href="" class="">
                    <i class="fas fa-bars"></i> Danh mục sách <i class="fas fa-chevron-down"></i>
                    </a>
                </div>
                <ul class="navbar-nav ml-auto top-right">
                    <li><span><i class="fas fa-phone-alt"></i> 0989 989 989</span></li>
                    <div class="gach-01"></div>
                    <li><span><i class="fas fa-comments"></i> <a href="">Hỗ trợ trực tuyến</a></span></li>
                </ul>
            </div>
        </div>

        <main class="">
            @yield('content')
        </main>
    </div>
</body>
</html>
