@extends('layouts.apppage')
@section('content')
<div class="slide d-flex align-items-center">
    <div class="main-slide ">
        <h3>Find & compare study programs abroad</h3>
        <h1>Where will your studies take you?</h1>
        <div>
            <span>
                Every year, our search engine helps over 7 million students find, compare, and connect with some of the best universities and schools around the world. Start your search today!
            </span>
        </div>
    </div>
</div>

@endsection
