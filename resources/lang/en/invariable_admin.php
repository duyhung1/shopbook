<?php

return [
    'header' => [
        'admin' => 'Admin',
        'profile_user' => 'Profile',
        'setting_user' => 'Setting',
        'login_user' => 'Login',
    ],
    'menu' => [
        'dasboard' => 'Dasboard',
        'user' => 'User',
        'list_user' => 'List User',
        'add_user' => 'Create User',
        'organization' => 'Organization',
        'list_organization' => 'List Organization',
        'add_organization' => 'Create Organization',
        'category' => 'Category',
        'list_category' => 'List Category',
        'add_category' => 'Create Category',
        'level' => 'Level',
        'list_level' => 'List Level',
        'add_level' => 'Create Level',
        'program' => 'Program',
        'list_program' => 'List Product',
        'add_program' => 'Create Product',
        'bill' => 'Bill',
        'news' =>'News',
        'list_news' => 'List News',
        'add_news' => 'Create News',
        'image' => 'Image',
        'list_image' => 'List Image',
        'add_image' => 'Create Image',
    ]
]

?>
