<?php

return [
    'header' => [
        'admin' => 'Quản trị',
        'profile_user' => 'Quản lý tài khoản',
        'setting_user' => 'Cài đặt',
        'login_user' => 'Đăng xuất',
    ],
    'menu' => [
        'dasboard' => 'Bảng điều khiển',
        'user' => 'Người dùng',
        'list_user' => 'Danh sách người dùng',
        'add_user' => 'Tạo người dùng',
        'organization' => 'Trường/Trung tâm',
        'list_organization' => 'Danh sách Trường/Trung tâm',
        'add_organization' => 'Tạo Trường/Trung tâm',
        'category' => 'Thể loại',
        'list_category' => 'Danh sách Thể loại',
        'add_category' => 'Tạo Thể loại',
        'level' => 'Cấp độ',
        'list_level' => 'Danh sách Cấp độ',
        'add_level' => 'Tạo Cấp độ',
        'program' => 'Chương trình',
        'list_program' => 'Danh sách sản phẩm',
        'add_program' => 'Tạo sản phẩm',
        'bill' => 'Đơn hàng',
        'news' =>'Tin tức',
        'list_news' => 'Danh sách tin tức',
        'add_news' => 'Thêm tin tức',
        'image' => 'Hình ảnh',
        'list_image' => 'Danh sách hình ảnh',
        'add_image' => 'Thêm hình ảnh',
    ]
]

?>
