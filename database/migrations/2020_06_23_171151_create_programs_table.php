<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProgramsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programs', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('organization_id');
            $table->string('location');
            $table->string('length');
            $table->string('study_pace');
            $table->string('education_level');
            $table->double('school_free');
            $table->date('start_date');
            $table->string('ref_link');
            $table->text('description');
            $table->string('admission');
            $table->string('language');
            $table->text('content');
            $table->string('qualification');
            $table->string('career_path');
            $table->string('root_source');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('programs');
    }
}
