<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();
        DB::table('role_user')->truncate();

        $adminRole = Role::where('name', 'admin')->first();
        $universityRole = Role::where('name', 'university')->first();
        // $university_subRole = Role::where('name', 'university_sub')->first();

        $admin = User::create([
            'name' => 'AdminMaster',
            'email' => 'admin@admin.com',
            'password' => Hash::make('adminadmin')
        ]);

        $university = User::create([
            'name' => 'University_01',
            'email' => 'university01@admin.com',
            'password' => Hash::make('adminadmin')
        ]);

        $admin->roles()->attach($adminRole);
        $university->roles()->attach($universityRole);
    }
}
