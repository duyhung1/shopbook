<?php

use Illuminate\Database\Seeder;
use App\Location;

class LocationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Location::truncate();

        Location::create(['name' => 'Việt Nam']);
        Location::create(['name' => 'Singapore']);
        Location::create(['name' => 'American']);
        Location::create(['name' => 'Japan']);
        Location::create(['name' => 'Korea']);
        Location::create(['name' => 'England']);
        Location::create(['name' => 'Germany']);
        Location::create(['name' => 'Italy']);
        Location::create(['name' => 'France']);
        Location::create(['name' => 'Russia']);
        Location::create(['name' => 'China']);
    }
}
