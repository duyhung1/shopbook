<?php

use App\Http\Controllers\IndexController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('/', 'IndexController');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::namespace('Admin')->prefix('admin')->name('admin.')->middleware('can:manage-users')->group(function() {
    Route::get('', 'AdminController@index')->name('index');
    Route::get('lang/{lang}',['as'=>'lang', 'uses'=>'LangController@changeLang']);

    Route::resource('users', 'UserController', ['except' => ['show']])->middleware('can:manage-organization');

    Route::resource('organization', 'OrganizationController', ['except' => ['show']])->middleware('can:manage-organization');

    Route::resource('category', 'CategoryController', ['except' => ['show']])->middleware('can:access_useradmin');

    Route::resource('level', 'LevelController', ['except' => ['show']])->middleware('can:access_useradmin');

    Route::resource('program', 'ProgramController', ['except' => ['show']]);

    Route::resource('logo', 'LogoController');
});
